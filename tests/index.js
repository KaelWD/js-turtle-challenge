const test   = require('tape');
const turtle = require('../src');

test('Move and turn', assert => {
    assert.same(turtle.run(
        ['move', 'turn', 'move']
    ), [1, 1, 'E']);
    assert.end();
});

test('Basic loop', assert => {
    assert.same(turtle.run(
        ['repeat', '2', 'move', 'end', 'turn', 'move']
    ), [1, 2, 'E']);
    assert.end();
});

test('Zero length loop', assert => {
    assert.throws(() => {
        return turtle.run(['repeat', '0', 'move', 'end'])
    });
    assert.end();
});

test('Nested loops', assert => {
    assert.same(turtle.run(
        ['repeat', '3', 'turn', 'repeat', '2', 'move', 'end', 'end', 'repeat', '5', 'move', 'end']
    ), [-5, -2, 'W']);
    assert.end();
});

test('Deeply nested loops', assert => {
    assert.same(turtle.run(
        ['repeat', '5', 'repeat', '5', 'repeat', '5', 'move', 'end', 'turn', 'end', 'move', 'end', 'move']
    ), [2, 5, 'E']);
    assert.end();
});

test('Program as a string', assert => {
    assert.same(turtle.run(
        `
        move
        repeat 3
            repeat 2
                move
            end
            turn
        end
        turn
        `
    ), [2, 1, 'N']);
    assert.end();
});

test('Execution time', assert => {
    let start = process.hrtime();
    start     = start[0] * 1000000 + start[1] / 1000;

    turtle.run(
        ['repeat', '15', 'repeat', '15', 'repeat', '15', 'move', 'turn', 'move', 'end', 'end', 'end']
    );

    let end = process.hrtime();
    end     = end[0] * 1000000 + end[1] / 1000;
    assert.comment('Run time ' + Math.round(end - start) / 1000 + 'ms');
    assert.end();
});
