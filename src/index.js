const turnMap = {
    N: 'E',
    E: 'S',
    S: 'W',
    W: 'N'
};

const forwardMap = {
    N: [0, 1],
    E: [1, 0],
    S: [0, -1],
    W: [-1, 0]
};

module.exports = {
    run(program, start = [0, 0, 'N']) {

        if (typeof program === 'string') program = program.trim().split(/\s+/);

        let length   = program.length;
        let pos      = 0;
        let stack    = [];
        let location = start;

        while (pos < length) {

            if (program[pos] === 'move') {

                forwardMap[location[2]].forEach((value, index) => {
                    location[index] += value;
                });

            } else if (program[pos] === 'turn') {

                location[2] = turnMap[location[2]];

            } else if (program[pos] === 'repeat') {

                if (program[pos + 1] < 1) throw `Repeat must run at least once at position ${pos + 1}`;

                stack.push({
                    pos: pos + 1,
                    loops: program[pos + 1]
                });

                ++pos;

            } else if (program[pos] === 'end' && stack.length > 0) {

                let currentLoop = stack[stack.length - 1];

                if (currentLoop.loops > 1) {
                    --currentLoop.loops;
                    pos = currentLoop.pos;
                } else {
                    stack.pop();
                }

            } else {
                throw `Unexpected token '${program[pos]}' at position ${pos + 1}`;
            }

            ++pos;

        }

        if (stack.length > 0) throw `Unclosed loop at position ${stack.pop().pos}`;

        return location;
    },
};
